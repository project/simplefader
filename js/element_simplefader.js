/**
 * @file
 * Javascript functions for Element SimpleFader.
 */

(function ($) {
  "use strict";

  function element_simplefader_function(context, element) {
    $(element, context).fadeOut( "fast", function() {
      $( this ).fadeIn( 5000 );
    });
  }

  Drupal.behaviors.element_simplefader = {
    attach: function (context, settings) {
      element_simplefader_function(context, Drupal.settings['element_simplefader_element']);
    }
  };
})(jQuery);
