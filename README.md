#CONTENTS OF THIS FILE#

- Introduction
- Requirements
- Recommended modules
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers

##INTRODUCTION##

The Element SimpleFader module takes a CSS selector in the configuration and
uses some simple jQuery to FadeIn when the page loads.

For a full description of the module, visit the project page:
https://www.drupal.org/sandbox/elliotc/2587609

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/2587609

##REQUIREMENTS##

No special requirements

##RECOMMENDED MODULES##

No recommended modules. However, this was tested using Omega 3 theme.

Omega Theme 7.x-3.1
https://www.drupal.org/project/omega

##INSTALLATION##

Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

##CONFIGURATION##

The module has a one-field configuration
/admin/config/user-interface/element_simplefader

Fill in the CSS selector that you would like to have FadeIn. No options.

It's simple, but you'll need to know how CSS works.

##TROUBLESHOOTING##

Make certain you have the correct CSS selector for your intended result.

##FAQ##

Q: Why aren't there more options? I'd like to FadeOut, Bounce, Rotate!
A: This is meant to be a very simple module. It did what I needed, and I
   didn't readily see another module that would handle this for me, and I
   didn't want to make a one-off custom module that wouldn't get shared
   back to the community.

##MAINTAINERS##

Current maintainers:

- Elliot Christenson (elliotc) - https://www.drupal.org/u/elliotc
